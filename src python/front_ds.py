import random

import PySimpleGUI as sg
import requests
import xmltodict
import leather


def colorizer(d):
    return 'rgb(%i, %i, %i)' % (d.x, d.y, 150)


sg.change_look_and_feel('Purple')
# All the stuff inside your window.
sensor_data = sg.Multiline("", size=(120, 20))
not_normal = ""
image = sg.Image(filename='')
layout = [[sg.Text('Hello Doctor!')],
          [sg.Text('Press the button corresponding to what you want to do')],
          [sg.Button('See patient activity')],
          [sg.Button('See patient took medication'), sg.InputText("Patient Id"), sg.InputText("medPlan Id")],
          [sg.Button('See patient not normal activities')],
          [sg.Button('Annotate normal activity'), sg.InputText("Activity Id")],
          [sg.Button('Send Recommendation'), sg.InputText("Recommendation here"), sg.InputText("Patient Id")],
          [sensor_data]
          ]

logged_in = False
layout_logged_out = [[sg.Text('Username'), sg.InputText()],
                     [sg.Text('Password'), sg.InputText(password_char='*')],
                     [sg.Button('Log in')]]

# Create the Window
logged_out_window = sg.Window('Welcome!', layout_logged_out)
once = True

# Event Loop to process "events" and get the "values" of the inputs
while True:

    if logged_in:
        if once:
            once = False
            window = sg.Window('Welcome', layout)
        event, values = window.read()

        if event in (None, 'See patient activity'):
            URL = "http://localhost:8080/ws/"
            body = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                      xmlns:loan="http://www.example.com/springdemo/soap">
                        <soapenv:Header/>
                        <soapenv:Body>
                          <loan:DoctorActivityRequest>
                              <loan:patientId>3</loan:patientId>
                          </loan:DoctorActivityRequest>
                        </soapenv:Body>
                    </soapenv:Envelope>'''
            header = {"Content-Type": "text/xml"}
            response = requests.post(URL, data=body, headers=header)
            parse_data = xmltodict.parse(response.content)
            parse_data = parse_data["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:DoctorActivityResponse"][
                "ns2:senzorData"]
            new_parse_data = {p.replace('null', '') for p in parse_data}
            sensor_data.update('\n'.join(new_parse_data))
            activity_frequency = {}
            for data in parse_data:
                activity = data.split(' ')[0]
                if activity not in activity_frequency.keys():
                    activity_frequency[activity] = 1
                else:
                    activity_frequency[activity] += 1

            data = []
            for key in activity_frequency.keys():
                data.append((key, activity_frequency[key]))
            chart = leather.Chart('Simple pairs')
            chart.add_columns(data)
            chart.to_svg('fig1.svg')
            print(parse_data)

        if event in (None, 'See patient took medication'):
            URL = "http://localhost:8080/ws/"
            body = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                      xmlns:loan="http://www.example.com/springdemo/soap">
                        <soapenv:Header/>
                        <soapenv:Body>
                               <loan:DoctorMedTakenRequest>
                               <loan:patientId>''' + values[0] + '''</loan:patientId>
                               <loan:medPlanId>''' + values[1] + '''</loan:medPlanId>
                               </loan:DoctorMedTakenRequest>
                        </soapenv:Body>
                    </soapenv:Envelope>'''
            header = {"Content-Type": "text/xml"}
            response = requests.post(URL, data=body, headers=header)
            parse_data = xmltodict.parse(response.content)
            parse_data = parse_data["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:DoctorMedTakenResponse"]["ns2:medPlan"]
            sensor_data.update(parse_data)

        if event in (None, 'See patient not normal activities'):
            URL = "http://localhost:8080/ws/"
            body = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                             xmlns:loan="http://www.example.com/springdemo/soap">
                               <soapenv:Header/>
                               <soapenv:Body>
                                 <loan:DoctorActivityRequest>
                                     <loan:patientId>3</loan:patientId>
                                 </loan:DoctorActivityRequest>
                               </soapenv:Body>
                           </soapenv:Envelope>'''
            header = {"Content-Type": "text/xml"}
            response = requests.post(URL, data=body, headers=header)
            parse_data = xmltodict.parse(response.content)
            parse_data = parse_data["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:DoctorActivityResponse"][
                "ns2:senzorData"]
            sensor_data.update('\n'.join(parse_data))

            for data in parse_data:
                if "YES" in data:
                    new_d = data.replace('null', '')
                    not_normal += new_d + "\n"
            sensor_data.update(not_normal)

        if event in (None, 'Annotate normal activity'):
            URL = "http://localhost:8080/ws/"
            body = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                                 xmlns:loan="http://www.example.com/springdemo/soap">
                                   <soapenv:Header/>
                                   <soapenv:Body>
                                     <loan:AnnotateActivityRequest>
                                          <loan:activityId>''' + values[2] + '''</loan:activityId>
                                     </loan:AnnotateActivityRequest>    
                                   </soapenv:Body>
                               </soapenv:Envelope>'''
            header = {"Content-Type": "text/xml"}
            response = requests.post(URL, data=body, headers=header)
            parse_data = xmltodict.parse(response.content)
            parse_data = parse_data["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:AnnotateActivityResponse"][
                "ns2:activityAnnotation"]
            sensor_data.update(parse_data)

        if event in (None, 'Send Recommendation'):
            URL = "http://localhost:8080/ws/"
            body = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                                 xmlns:loan="http://www.example.com/springdemo/soap">
                                   <soapenv:Header/>
                                   <soapenv:Body>
                                     <loan:AddRecommendationRequest>
                                        <loan:recommendation>''' + values[3] + '''</loan:recommendation>
                                        <loan:doctorId>1</loan:doctorId>
                                        <loan:patientId>''' + values[4] + '''</loan:patientId>
                                    </loan:AddRecommendationRequest>   
                                   </soapenv:Body>
                               </soapenv:Envelope>'''
            header = {"Content-Type": "text/xml"}
            response = requests.post(URL, data=body, headers=header)
            parse_data = xmltodict.parse(response.content)
            parse_data = parse_data["SOAP-ENV:Envelope"]["SOAP-ENV:Body"]["ns2:AddRecommendationResponse"][
                "ns2:recommendation"]
            sensor_data.update(parse_data)
    else:
        event, values = logged_out_window.read()

        if event in (None, 'Log in') and 'Doctor1' in values[0] and 'd1' in values[1]:
            logged_in = True
            logged_out_window.hide()

window.close()
