package com.example.springdemo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
public class Recommendation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer patientId;
    private Integer doctorId;

    private String recommendation;

    public Recommendation(Integer patientId, Integer doctorId, String recommendation){
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.recommendation = recommendation;
    }

    public String toString(){
        return recommendation;
    }


}
