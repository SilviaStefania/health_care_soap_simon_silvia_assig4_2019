package com.example.springdemo.entities;

public class Roles {
    public static final String DOCTOR = "DOCTOR";
    public static final String PATIENT = "PATIENT";
    public static final String CAREGIVER = "CAREGIVER";
}
