package com.example.springdemo.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "medication")

public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medId;
    private String medName;
    private String dosage;
    private String sideEffects;

    @ManyToMany(mappedBy = "medications")
    private List<MedicalPlan> medicalPlans = new ArrayList<>();

    public Medication(Integer medId, String medName, String dosage, String sideEffects) {
        this.medId = medId;
        this.medName = medName;
        this.dosage = dosage;
        this.sideEffects = sideEffects;
    }

    public String toString() {
        return this.medName + " " + this.dosage + " " + this.sideEffects + "; ";
    }

}
