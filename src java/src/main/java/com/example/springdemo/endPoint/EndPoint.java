package com.example.springdemo.endPoint;


import com.example.springdemo.services.SoapService;
import com.example.springdemo.soap.*;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class EndPoint {

    private static final String NAMESPACE = "http://www.example.com/springdemo/soap";

    private final SoapService soapService;

    @PayloadRoot(namespace = NAMESPACE, localPart = "DoctorActivityRequest")
    @ResponsePayload
    public DoctorActivityResponse getActivities(@RequestPayload DoctorActivityRequest activityRequest){
        return soapService.getActivities(activityRequest);
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "DoctorMedTakenRequest")
    @ResponsePayload
    public DoctorMedTakenResponse getMedTaken(@RequestPayload DoctorMedTakenRequest medTakenRequest) {
        return soapService.getMedTaken(medTakenRequest);
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "AddRecommendationRequest")
    @ResponsePayload
    public AddRecommendationResponse getAddRec(@RequestPayload AddRecommendationRequest recommendationRequest){
        return soapService.getAddRec(recommendationRequest);
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "AnnotateActivityRequest")
    @ResponsePayload
    public AnnotateActivityResponse getAnnotateAct(@RequestPayload AnnotateActivityRequest annotateActivityRequest){
        return soapService.getAnnotateAct(annotateActivityRequest);
    }


}
