package com.example.springdemo.repositories;

import com.example.springdemo.entities.Recommendation;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface RecommendationRepository extends Repository<Recommendation, Integer> {
    Recommendation save(Recommendation recommendation);
    List<Recommendation> findAll();
    Optional<Recommendation> findById(int id);
    void delete(Recommendation recommendation);
}
