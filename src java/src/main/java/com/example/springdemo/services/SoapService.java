package com.example.springdemo.services;


import com.example.springdemo.entities.Activity;
import com.example.springdemo.entities.MedicalPlan;
import com.example.springdemo.entities.Recommendation;
import com.example.springdemo.repositories.RepositoryFactory;
import com.example.springdemo.soap.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SoapService {

    private final RepositoryFactory repositoryFactory;

    public DoctorActivityResponse getActivities(DoctorActivityRequest activityRequest){
        DoctorActivityResponse doctorActivityResponse = new DoctorActivityResponse();
        List<String> activities = doctorActivityResponse.getSenzorData();
        repositoryFactory.createActivityRepository().findAll().forEach(e->
             activities.add(e.toString()) );
        return doctorActivityResponse;
    }

    public DoctorMedTakenResponse getMedTaken(DoctorMedTakenRequest medTakenRequest) {
        DoctorMedTakenResponse doctorMedTakenResponse = new DoctorMedTakenResponse();
        MedicalPlan med = repositoryFactory.createMedicalPlanRepository().findById(medTakenRequest.getMedPlanId()).get();
        doctorMedTakenResponse.setMedPlan(med.toString());
        return doctorMedTakenResponse;
    }

    public AddRecommendationResponse getAddRec(AddRecommendationRequest recommendationRequest){
        AddRecommendationResponse addRecommendationResponse = new AddRecommendationResponse();
        Recommendation recommendation = new Recommendation(recommendationRequest.getPatientId(), recommendationRequest.getDoctorId(), recommendationRequest.getRecommendation());
        addRecommendationResponse.setRecommendation(repositoryFactory.createRecommendationRepository().save(recommendation).toString());
        return addRecommendationResponse;
    }

    public AnnotateActivityResponse getAnnotateAct(AnnotateActivityRequest annotateActivityRequest){
        AnnotateActivityResponse annotateActivityResponse = new AnnotateActivityResponse();
        Activity act =   repositoryFactory.createActivityRepository().findById(annotateActivityRequest.getActivityId()).get();
        act.setAnnotation("Normal Activity");
        annotateActivityResponse.setActivityAnnotation("Activity marked as normal");
        repositoryFactory.createActivityRepository().save(act);
        return annotateActivityResponse;
    }




}
